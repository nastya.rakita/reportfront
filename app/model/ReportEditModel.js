Ext.define('ReportFront.model.ReportEditModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.reportmodel',

    stores: {
        myStore: {
            type: 'reportdata'
        }
    },
    record: null,
});