Ext.define('ReportFront.model.ReportData', {
    extend: 'Ext.data.Model',
    idProperty: 'myId',
    fields: [
        { name: 'id', type: 'int', mapping: 'myId', },
        { name: 'name', type: 'string' },
        { name: 'description', type: 'string' },
        { name: 'private', type: 'boolean' },
        { name: 'kpiList', type: 'auto' }
    ]
});