Ext.define('ReportFront.view.main.ReportList', {
    extend: 'Ext.grid.Grid',
    xtype: 'reportlist',
    id: "reportlist",
    controller: 'reportmodel',
    title: 'Reports',
    store: {
        type: 'reportdata'
    },
    items: [{
        xtype: 'toolbar',
        dock: 'top',
        items: [{
            xtype: 'button',
            text: 'Create',
            handler: function() {
                Ext.create('ReportFront.view.main.ReportEdit', {
                    record: Ext.create('ReportFront.model.ReportData').data
                }).show();
            }
        }]
    }],
    columns: [{
            text: 'Id',
            dataIndex: 'id',
            flex: 1
        }, {
            text: 'Private',
            dataIndex: 'private',
            flex: 1
        }, {
            text: 'Title',
            dataIndex: 'name',
            flex: 1
        }, {
            text: 'Description',
            dataIndex: 'description',
            flex: 1
        }, {
            text: 'KPI',
            dataIndex: 'kpiList',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            flex: 1,
            width: 80,
            dataIndex: 'button',
            cell: {
                xtype: 'widgetcell',
                widget: {
                    xtype: 'button',
                    text: 'Edit',
                    width: '100%',
                    handler: function(button, e) {
                        let data = this.up().up()._record.data;
                        Ext.create('ReportFront.view.main.ReportEdit', {
                            record: data
                        }).show();
                    }
                }
            }
        }
    ],
    renderTo: Ext.getBody(),
});