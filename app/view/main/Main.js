Ext.define('ReportFront.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    title: 'Home',
    layout: 'fit',
    tabBarPosition: 'bottom',
    items: [
        {
            xtype: 'reportlist',
        }]
});
