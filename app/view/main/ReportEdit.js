Ext.define('ReportFront.view.main.ReportEdit', {
    extend: 'Ext.window.Window',
    alias: 'widget.reportedit',
    xtype: 'reportedit',
    controller: 'reportmodel',
    layout: 'fit',
    modal: false,
    width: 400,
    isFormField: true,
    height: 350,
    closable: true,
    viewModel: {
        links: {
            record: {
                type: 'ReportFront.model.ReportData',
                create: true
            }
        }
    },
    constructor: function(record) {
        Ext.applyIf(this, record);
        this.callParent();
    },


    items: {
        xtype: 'panel',
        items: [{
                xtype: 'textfield',
                reference: 'name',
                label: 'Title',
                bind: '{record.name}'
            }, {
                xtype: 'textfield',
                reference: 'description',
                label: 'Description',
                bind: '{record.description}'
            }, {
                xtype: 'checkboxfield',
                label: 'Private',
                labelWidth: '95%',
                labelAlign: 'right',
                name: 'private',
                bind: {
                    value: {
                        bindTo: '{record.private}',
                        deep: true
                    },

                    checked: '{record.private}',
                },

            },
            {
                xtype: 'fieldset',
                multiSelect: true,
                fieldLabel: 'KPI list',
                items: [{
                    xtype: 'selectfield',
                    multiSelect: true,
                    options: [{
                            text: 'TWAMP_Availability',
                            value: 'TWAMP_Availability'
                        },
                        {
                            text: 'TWAMP_Jitter',
                            value: 'TWAMP_Jitter'
                        },
                        {
                            text: 'TWAMP_Jitter_Far',
                            value: 'TWAMP_Jitter_Far'
                        },
                        {
                            text: 'TWAMP_Jitter_Near',
                            value: 'TWAMP_Jitter_Near'
                        },
                        {
                            text: 'TWAMP_Latency',
                            value: 'TWAMP_Latency'
                        },
                        {
                            text: 'TWAMP_Latency_Far',
                            value: 'TWAMP_Latency_Far'
                        },
                        {
                            text: 'TWAMP_Latency_Near',
                            value: 'TWAMP_Latency_Near'
                        },
                        {
                            text: 'TWAMP_Packets',
                            value: 'TWAMP_Packets'
                        },
                        {
                            text: 'TWAMP_Tests',
                            value: 'TWAMP_Tests'
                        },
                        {
                            text: 'TWAMP_Thresholds',
                            value: 'TWAMP_Thresholds'
                        },
                    ],
                    bind: '{record.kpiList}',
                    label: 'Select KPI'
                }]
            }
        ]
    },
    buttons: [{
        text: 'Save',
        handler: function(button) {
            button.fireEvent('buttonPush', this, button);
        },
        reference: 'buttonsave',
    }, {
        text: 'Cancel',
        handler: 'onCancelClick'
    }],
    listeners: {
        show: 'onEditClick'
    }

});