Ext.define('ReportFront.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.reportmodel',
    requires: ['ReportFront.view.main.ReportEdit'],
    store: ['ReportFront.store.ReportStore'],
    control: {
        'button[reference=buttonsave]': {
            buttonPush: 'onSaveClick',
        }
    },
    onEditClick: function(win) {
        var view = this.getView(),
            vm = view.getViewModel(),
            rec = view.record;
        vm.set('record', rec);
    },

    onSaveClick: function(record, button) {
        var view = this.getView(),
            values = view.record;
        body = {
            name: values.name,
            description: values.description,
            private: values.private,
            kpiList: values.kpiList
        };
        if (values.id == 0) {
            Ext.Ajax.request({
                url: 'http://localhost:8080/report',
                method: 'POST',
                jsonData: body,
                success: function(response) {
                    var data = Ext.decode(response.responseText);
                    if (data == true) {
                        let alertBox = Ext.Msg.alert('Success');
                        var grid = Ext.getCmp("reportlist");
                        store = grid.getStore();
                        store.reload();
                        setTimeout(function() {
                                alertBox.hide();
                                view.close();
                            },
                            2000);
                    } else {
                        let alertBox = Ext.Msg.alert('Failed');
                        setTimeout(function() {
                                alertBox.hide();
                                view.close();
                            },
                            2500);

                    }
                }
            })
        } else {
            body.id = values.id,
                Ext.Ajax.request({
                    url: 'http://localhost:8080/report',
                    method: 'PUT',
                    jsonData: body,
                    success: function(response) {
                        var data = Ext.decode(response.responseText);
                        if (data == true) {
                            let alertBox = Ext.Msg.alert('Success');
                            var grid = Ext.getCmp("reportlist");
                            store = grid.getStore();
                            store.reload();
                            setTimeout(function() {
                                    alertBox.hide();
                                    view.close();
                                },
                                2000);
                        } else {
                            let alertBox = Ext.Msg.alert('Failed');
                            setTimeout(function() {
                                    alertBox.hide();
                                    view.close();
                                },
                                2500);

                        }
                    }
                })
        }
    },

    onCancelClick: function() {
        this.getView().close();
    },
});