Ext.define('ReportFront.store.ReportStore', {
    extend: 'Ext.data.Store',
    alias: 'store.reportdata',
    model: 'ReportFront.model.ReportData',
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'http://localhost:8080/report',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        }
    }

});