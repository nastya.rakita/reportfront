Ext.application({
    extend: 'ReportFront.Application',
    stores: ['ReportFront.store.ReportStore'],
    name: 'ReportFront',

    requires: [
        'ReportFront.*',
        'Ext.grid.*',
        'Ext.data.*',
        'Ext.util.*'
    ],

    mainView: 'ReportFront.view.main.ReportList'
});